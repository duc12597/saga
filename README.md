Client NodeJS cho Saga test kết nối với Keycloak
------------------------------------------------

1. Clone repo
-------------

    git clone https://duc12597@bitbucket.org/duc12597/saga.git
    cd saga

2. Cài đặt các dependency
-------------------------
    
    npm install

3. Chạy client
--------------

    node app.js

4. Truy cập [http://localhost:9000](http://localhost:9000)
-----------------------------------

5. Login bằng Google
--------------------
- Bấm nút login bằng Google và nhập tài khoản Gmail
- **NOTE:** Login bằng Facebook đang bị lỗi
