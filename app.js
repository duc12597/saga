//  FIXME: DANGEROUS: This disables HTTPS/SSL/TLS checking across the entire node.js environment
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

// Initialise 
const Keycloak = require('keycloak-connect');
const express = require('express');
const session = require('express-session');
const app = express();
// Use Pug template engine
app.set('view engine', 'pug');
// Configure logging
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'debug';

const server = app.listen(9000, function () {
	var msg = 'Example at http://localhost:' + 9000;
	logger.debug(msg);
	logger.info(msg);
	logger.warn(msg);
	logger.error(msg);
	logger.fatal(msg);
});

// Create a session-store to be used by both the express-session middleware and the keycloak middleware.
var memoryStore = new session.MemoryStore();
app.use(session({
	secret: 'secret',
	resave: false,
	saveUninitialized: true,
	store: memoryStore
}));

// Provide the session store to the Keycloak so that sessions can be invalidated from the Keycloak console callback.
// Additional configuration is read from keycloak.json.
var keycloak = new Keycloak({
	store: memoryStore
});

// Install the Keycloak middleware.
app.use(keycloak.middleware({
	logout: '/logout',
	admin: '/'
}));

app.get('/', function (req, res) {
	res.render('index', { 
		title: 'Home' 
	});
});

app.get('/topic/:id', keycloak.protect(), function (req, res) {
	var topicid = req.params.id;
	res.render('topic', {
		title: 'Topic ' + topicid,
		topic: topicid, 
	});
});

app.get('/post/:author/:id', keycloak.protect(), function (req, res) {
	res.render('post', { 
		title: req.params.id,
		post: req.params.id,
		author: req.params.author,
	});
});

app.get('/author/:id', keycloak.protect(), function (req, res) {
	res.render('author', {
		title: req.params.id,
		author: req.params.id,
	});
});

app.get('/profile/', keycloak.protect(), function (req, res) {
	res.render('profile', {
		title: req.params.id + ' Profile',
		profile: req.params.id,
	});
	logger.debug(JSON.stringify(JSON.parse(req.session['keycloak-token']), null, 4));
});

// app.get('/ca', keycloak.protect('lvl3'), function (req, res) {
// 	res.send('ca');
// });

app.get('/ca', function (req, res) {
	res.send('ca');
});